angular.module('todoapp').controller('todosCtrl', function ($scope, $routeParams, todoStore) {

	$scope.todoToAdd = '';

	$scope.todos = todoStore.get();

	$scope.$watch('todos', function () {
		$scope.completedCount = $scope.todos.filter(function (x) { return x.completed; }).length;
		$scope.remainingCount = $scope.todos.length - $scope.completedCount;
		$scope.allChecked = !$scope.remainingCount;
	}, true);

	$scope.$on('$routeChangeSuccess', function () {
		$scope.status = $routeParams.status || '';

		if ($scope.status === 'active') {
			$scope.statusFilter = { completed: false };
		}
		else if ($scope.status === 'completed') {
			$scope.statusFilter = { completed: true };
		}
		else {
			$scope.statusFilter = null;
		}
	});

	$scope.addTodo = function () {
		if ($scope.todoToAdd) {
			$scope.todos.push({
				text: $scope.todoToAdd,
				completed: false
			});
			todoStore.save($scope.todos);
			$scope.todoToAdd = '';
		}
	};

	$scope.removeTodo = function (todo) {
		$scope.todos.splice($scope.todos.indexOf(todo), 1);
		todoStore.save($scope.todos);
	};

	$scope.editTodo = function (todo) {
		$scope.editedTodo = todo;
		$scope.originalTodo = angular.copy(todo);
	}

	$scope.saveEdit = function () {
		if ($scope.editedTodo.text) {
			$scope.editedTodo = null;
			$scope.originalTodo = null;
			todoStore.save($scope.todos);
		}
	}

	$scope.revertEdit = function () {
		$scope.editedTodo.text = $scope.originalTodo.text;
		$scope.editedTodo.completed = $scope.originalTodo.completed;

		$scope.editedTodo = null;
		$scope.originalTodo = null;
	}

	$scope.clearCompleted = function () {
		var i, todo;
		for(i = $scope.todos.length - 1; i >= 0; i--) {
			todo = $scope.todos[i];
			if (todo.completed) {
				$scope.removeTodo(todo);
			}
		}
	}

	$scope.markAll = function (completed) {
		$scope.todos.forEach(function (todo) {
			todo.completed = completed;
		});
	};

});
