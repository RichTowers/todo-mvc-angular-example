describe('TodoCtrl', function () {

	beforeEach(module('todoapp'));
	beforeEach(inject(function ($controller, $rootScope) {
		var mockTodoStore = {
			get: function () { return []; },
			save: angular.noop
		};
		this.routeParams = {};
		this.scope = $rootScope.$new();
		this.ctrl = $controller('todosCtrl', { $scope: this.scope, todoStore: mockTodoStore, $routeParams: this.routeParams });
	}));

	it('should have no items initially', function () {
		expect(this.scope.todos.length).toBe(0);
	});

	it('should add a todo', function () {
		this.scope.todoToAdd = 'Test a simple case';
		this.scope.addTodo();
		expect(this.scope.todos.length).toBe(1);
		expect(this.scope.todos[0].text).toBe('Test a simple case');
	});

	it('should not add an empty todo', function () {
		this.scope.todoToAdd = '';
		this.scope.addTodo();
		expect(this.scope.todos.length).toBe(0);
	});

	it('should add multiple todos', function () {
		this.scope.todoToAdd = 'Test a simple case';
		this.scope.addTodo();
		this.scope.todoToAdd = 'Test a slightly more complex case';
		this.scope.addTodo();
		expect(this.scope.todos.length).toBe(2);
		expect(this.scope.todos[0].text).toBe('Test a simple case');
		expect(this.scope.todos[1].text).toBe('Test a slightly more complex case');
	});

	it('should clear the current todo after adding it', function () {
		this.scope.todoToAdd = 'Some value';
		this.scope.addTodo();
		expect(this.scope.todoToAdd).toBe('');
	});

	describe('when there is one todo', function () {

		beforeEach(function () {
			this.scope.todoToAdd = 'Already present todo';
			this.scope.addTodo();
		});

		it('should remove a todo', function () {
			expect(this.scope.todos.length).toBe(1);
			this.scope.removeTodo(this.scope.todos[0]);
			expect(this.scope.todos.length).toBe(0);
		});

		it('should count completed todos', function () {
			this.scope.$digest();
			expect(this.scope.completedCount).toBe(0);
			this.scope.todos[0].completed = true;
			this.scope.$digest();
			expect(this.scope.completedCount).toBe(1);
			expect(this.scope.allChecked).toBe(true);
		});

		it('should edit the todo', function () {
			this.scope.editTodo(this.scope.todos[0]);
			this.scope.editedTodo.text = 'BANANA';
			this.scope.saveEdit();
			expect(this.scope.todos[0].text).toBe('BANANA');
		});
	});

	describe('when there are multiple todos', function () {

		beforeEach(function () {
			this.scope.todos = [
				{name: '1', completed: false},
				{name: '2', completed: true },
				{name: '3', completed: true},
			];
		});

		it('should mark all as completed', function () {
			this.scope.markAll(true);
			var allCompleted = this.scope.todos.reduce(function (acc, item) { return item.completed && acc; }, true);
			expect(allCompleted).toBe(true);
		});

		it('should mark all as uncompleted', function () {
			this.scope.markAll(false);
			var noneCompleted = this.scope.todos.reduce(function (acc, item) { return !item.completed && acc; }, true);
			expect(noneCompleted).toBe(true);
		});

		it('should clear completed todos', function () {
			this.scope.clearCompleted();
			expect(this.scope.todos.length).toBe(1);
		});

		it('should filter based on route', function () {
			this.routeParams.status = '';
			this.scope.$broadcast('$routeChangeSuccess', {});

			expect(this.scope.status).toBe('');
			expect(this.scope.statusFilter).toBe(null);

			this.routeParams.status = 'active';
			this.scope.$broadcast('$routeChangeSuccess', {});

			expect(this.scope.status).toBe('active');
			expect(this.scope.statusFilter).toEqual({completed: false});

			this.routeParams.status = 'completed';
			this.scope.$broadcast('$routeChangeSuccess', {});

			expect(this.scope.status).toBe('completed');
			expect(this.scope.statusFilter).toEqual({completed: true});
		})
	});
});
