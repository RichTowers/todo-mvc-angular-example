angular.module('todoapp').factory('todoStore', function ($window) {
	var STORAGE_KEY = 'angularTodoStore';

	return {
		get: function () {
			return JSON.parse($window.localStorage.getItem(STORAGE_KEY) || '[]');
		},
		save: function (todos) {
			$window.localStorage.setItem(STORAGE_KEY, JSON.stringify(todos));
		}
	};
});