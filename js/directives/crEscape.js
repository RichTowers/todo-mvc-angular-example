angular.module('todoapp').directive('crEscape', function () {
	return function (scope, elem, attrs) {
		var ESCAPE_KEY = 27;
		elem.bind('keydown', function (event) {
			if (event.keyCode === ESCAPE_KEY) {
				scope.$apply(attrs.crEscape);
			}
		});
	};
});