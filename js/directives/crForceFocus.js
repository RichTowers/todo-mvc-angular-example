angular.module('todoapp').directive('crForcefocus', function ($timeout) {
	return function (scope, elem, attrs) {
		scope.$watch(attrs.crForcefocus, function (newVal) {
			if (newVal) {
				$timeout(function () { elem[0].focus(); }, 0, false);
			}
		});
	};
});