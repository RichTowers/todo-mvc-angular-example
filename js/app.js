angular.module('todoapp', ['ngRoute'])
	.config(function ($routeProvider) {
		$routeProvider
			.when('/', { controller: 'todosCtrl' })
			.when('/:status', { controller: 'todosCtrl' })
			.otherwise({ redirectTo: '/' });
	})
	.run(function ($route) { });
