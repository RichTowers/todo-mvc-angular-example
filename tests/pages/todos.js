function Todos () {
	this.mainSection = element(by.id('main'));
	this.footerSection = element(by.id('footer'));
	this.todoToAdd = element(by.model('todoToAdd'));
	this.todos = element.all(by.repeater('todo in todos'));
	this.todoCount = element(by.id('todo-count'));
	this.allChecked = element(by.model('allChecked'));
	this.completedCheckboxes = element.all(by.model('todo.completed'));
	this.editedTodo = element(by.model('editedTodo.text'));
	this.firstTodo = this.todos.get(0);
}
module.exports = Todos;