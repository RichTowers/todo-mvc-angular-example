var Todos = require('./pages/todos');

describe('Todo App', function() {

	// Setup

	beforeEach(function () {
		browser.get('http://localhost:8000');
		this.page = new Todos();
	});

	// TearDown

	afterEach(function () {
		browser.executeScript('window.localStorage.clear();');
	});

	afterEach(function () {
		browser.manage().logs().get('browser').then(function(browserLog) {
			expect(browserLog.length).toEqual(0);
			if (browserLog.length) {
				console.log('log: ' + require('util').inspect(browserLog));
			}
		});
	});

	// Tests

	it('should hide #main and #footer initially', function () {
		expect(this.page.mainSection.isDisplayed()).toBe(false);
		expect(this.page.footerSection.isDisplayed()).toBe(false);
	});

	it('should add a todo', function() {
		this.page.todoToAdd.sendKeys('Test the app');
		this.page.todoToAdd.sendKeys(protractor.Key.ENTER);
		expect(this.page.todos.count()).toBe(1);
		expect(this.page.todoToAdd.getAttribute('value')).toBe('');

		// Main and Footer should be shown now:
		expect(this.page.mainSection.isDisplayed()).toBe(true);
		expect(this.page.footerSection.isDisplayed()).toBe(true);

		expect(this.page.todoCount.getText()).toBe('1 item left');
	});

	it('should add multiple todos', function () {

		this.page.todoToAdd.sendKeys('Test the first todo');
		this.page.todoToAdd.sendKeys(protractor.Key.ENTER);

		this.page.todoToAdd.sendKeys('Test the second todo');
		this.page.todoToAdd.sendKeys(protractor.Key.ENTER);

		expect(this.page.todos.count()).toBe(2);
		expect(this.page.todoToAdd.getAttribute('value')).toBe('');
		expect(this.page.todoCount.getText()).toBe('2 items left');
	});

	it('should add multiple todos with the same name', function () {

		this.page.todoToAdd.sendKeys('Test same todo');
		this.page.todoToAdd.sendKeys(protractor.Key.ENTER);
		this.page.todoToAdd.sendKeys('Test same todo');
		this.page.todoToAdd.sendKeys(protractor.Key.ENTER);
	});

	it('should mark todo as completed', function () {

		this.page.todoToAdd.sendKeys('Test a todo');
		this.page.todoToAdd.sendKeys(protractor.Key.ENTER);

		this.page.completedCheckboxes.get(0).click();
		expect(this.page.firstTodo.getAttribute('class')).toMatch('completed');
		expect(this.page.allChecked.getAttribute('checked')).toBe('true');
	});

	it('should edit a todo', function () {

		this.page.todoToAdd.sendKeys('Test a todo');
		this.page.todoToAdd.sendKeys(protractor.Key.ENTER);

		browser.actions().doubleClick(this.page.firstTodo).perform();

		this.page.editedTodo.clear();
		this.page.editedTodo.sendKeys('BANANA');
		this.page.editedTodo.sendKeys(protractor.Key.ENTER);

		expect(this.page.firstTodo.getText()).toBe('BANANA');
	});

	it('should persist changes when the page is reloaded', function () {

		this.page.todoToAdd.sendKeys('Test a todo');
		this.page.todoToAdd.sendKeys(protractor.Key.ENTER);

		browser.get('http://localhost:8000');

		expect(this.page.todos.count()).toBe(1);
	});
});
